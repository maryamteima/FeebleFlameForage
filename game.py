#FIRE-QUEST
#By: Maryam Teima & Yara Badwi

#------ IMPORTING -------
import arcade

#------ CONSTANTS -------

SCREEN_WIDTH = 1400
SCREEN_HEIGHT = 760
SCREEN_TITLE = "FireQuest"
CHAR_SCALE = 1
TILE_SCALE = 0.5
PLAYER_X_SPEED = 8
PLAYER_Y_SPEED = 20
GRAVITY = 1

#WINDOW/APP CLASS
class Game(arcade.Window):
    #__init__ is the constructor
    #self is the intancing of the class --> allows access to methods and data members
    def __init__(self):
        
        #super() calls the parent class's methods to override it (for inheritance)
        #Super().__init__ calls both the parent constructor and child constructor
        #If code below was only __init__, then the child class doesn't call the parent class, it only creates the child constructor   
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
        
        #SCENE OBJECT (RENDERER)
        self.scene = None

        #SPRITE VARIABLES 
        #holds the curr sprite itself
        self.player_sprite = None

        #PHYSICS ENGINE (CAN UPGRADE TO PYMUNK FOR ADVANCED PHYSICS)
        self.phys_eng = None

        #CAMERA
        self.camera = None
        
        arcade.set_background_color(arcade.csscolor.BLACK)
    
    
    #GAME SETUP - (Used to restart game as well)
    def setup(self):
        #CAMERA SETUP
        self.camera = arcade.Camera(self.width, self.height)

        #SCENE SETUP
        #Creating a scene object to handle drawing level-sprites on-screen
        self.scene = arcade.Scene()
        
        #SPRITE LISTS
        #Used to keep track of sprites
        #Each sprite is contained in its own list

        #Holds only the player. If multiplayer game, this would hold Player#1 & Player#2
        self.scene.add_sprite_list("Players")
        
        #Wall_List holds all objects the player can't walk through (barrels, ground, fountains, etc)
        #Spatial-Hashing speeds up collision detection but slows down movement, since walls will be used in collision (and won't move) hashing is activated
        #player will constantly move thus hashing is turned off
        self.scene.add_sprite_list("Walls", use_spatial_hash=True)

        
        #PLAYER SETUP
        #If code gets too crowded, seperate this setup to a different class

        #Positioning at coordinates
        #:resources is aracde.py's built in sprites
        
        #***CHANGE PATH WHEN IMPORTING POLISHED CHARACTER SPRITE***
        img_src = ":resources:images/animated_characters/female_adventurer/femaleAdventurer_idle.png"
        self.player_sprite = arcade.Sprite(img_src,CHAR_SCALE)
       
        #Changing the sprite's pivot point to be its centre
        #***CHANGE VALUES ON IMPORT OF LARGER-POLISHED SPRITES***
        self.player_sprite.center_x = 64
        self.player_sprite.center_y = 128
        #Adding the player sprite into the "Players" list
        self.scene.add_sprite("Players", self.player_sprite)

        #GROUND SETUP
        #Uses for loop to place ground sprites horizontally (without manual inputting required)

        for x in range(0,1600,64):
            ground = arcade.Sprite(":resources:images/tiles/grassMid.png",TILE_SCALE)
            ground.center_x = x
            ground.center_y = 32
            #Adding the ground sprite to Walls list (immovable objects list)
            self.scene.add_sprite("Walls", ground)

        #ACCESSORIES SETUP (in this case crates, change later to game elements)
        
        #Coordinates for each crate, change the amount of nested [] to match how many elemnts to place on ground
        coord_list = [[1300,96],[256,96],[786,96]]

        for coord in coord_list:
            #***CHANGE FILE PATH WITH POLISHED SPRITE***
            accesory = arcade.Sprite(":resources:images/tiles/boxCrate_double.png", TILE_SCALE)
            accesory.position = coord
            
            #Adding the accesory sprite to wall list (immovable objects list)
            self.scene.add_sprite("Walls", accesory)

        #PHYSICS SETUP
        #syntax: collider, collided
        self.phys_eng = arcade.PhysicsEnginePlatformer(self.player_sprite, gravity_constant=GRAVITY, walls=self.scene["Walls"])

    #GAME RENDERER
    def on_draw(self):

        #return super().on_draw()
        #Clear the window every frame, similar to Js (avoids smearing of sprites)
        self.clear()

        #activating camera
        self.camera.use()

        #DRAWING SCENE (INCLDUES SPRITES)
        self.scene.draw()

    #USER INPUTS (WASD & ARROW KEYS)
    
    # ********* YARA *****************
    
    #when keypressed
    def on_key_press(self, key, modifiers):
        
        if key == arcade.key.UP or key == arcade.key.W:
            if self.phys_eng.can_jump():
                self.player_sprite.change_y = PLAYER_Y_SPEED
        elif key == arcade.key.DOWN or key == arcade.key.S:
            self.player_sprite.change_y = -PLAYER_X_SPEED
        elif key == arcade.key.LEFT or key == arcade.key.A:
            self.player_sprite.change_x = -PLAYER_X_SPEED
        elif key == arcade.key.RIGHT or key == arcade.key.D:
            self.player_sprite.change_x = PLAYER_X_SPEED
    #when key released (similar to HTML, creates continous player movement on press & stops when released)
        
    def on_key_release(self, key, modifiers):
        
        if key == arcade.key.UP or key == arcade.key.W:
            self.player_sprite.change_y = 0
        elif key == arcade.key.DOWN or key == arcade.key.S:
            self.player_sprite.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.A:
            self.player_sprite.change_x = 0
        elif key == arcade.key.RIGHT or key == arcade.key.D:
            self.player_sprite.change_x = 0
        
#CAMERA
    def camera_follow(self):
        #Camera Location relative to Screen
        #If wanted to change the size, change the division
        screen_Xcentre = self.player_sprite.center_x - (self.camera.viewport_width/2)
        screen_Ycentre = self.player_sprite.center_y - (self.camera.viewport_height/2)

        #Ensuring Camera doesn't exist screen. If it does exist, set the location to the boundary
        if screen_Xcentre<0:
            screen_Xcentre = 0
        if screen_Ycentre<0:
            screen_Ycentre = 0
        target_centred = screen_Xcentre, screen_Ycentre
        self.camera.move_to(target_centred)

#MOVEMENT & GAME UPDATE
    def on_update(self, delta_time):

        # Move player with physics engine
        self.phys_eng.update()

        #Activate Camera 
        self.camera_follow()

#MAIN/GAME-LOOP
def main():
    #Instancing the window as a Game Class object
    window = Game()
    window.setup()
    arcade.run()

#Ensures the main() is run as the main program, not as a standalone script
if __name__ == "__main__":
    main()

    